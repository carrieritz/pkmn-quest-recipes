PKMN Quest Recipes
==================

Tool for Pokémon Quest (a free-to-play Pokémon game on Nintendo Switch) that
provides an exhaustive list of all possible recipes.

Resources
---------

* `Source code`_
* `Reporting issues`_

License
-------

This project is licensed under the `MIT License`_.

.. _`Source code`: https://gitlab.com/carrieritz/pkmn-quest-recipes
.. _`Reporting issues`: https://gitlab.com/carrieritz/pkmn-quest-recipes/issues
.. _`MIT License`:
    https://gitlab.com/carrieritz/pkmn-quest-recipes/blob/master/LICENSE
